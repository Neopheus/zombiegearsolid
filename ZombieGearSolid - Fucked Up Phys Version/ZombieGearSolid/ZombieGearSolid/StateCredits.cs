﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZombieGearSolid
{

    public class StateCredits : GameState
    {
        public StateCredits(Game1 g1) : base(g1) { }
        private Vector2 position = new Vector2 (0, 480);


        override public void update(float timeSinceLastFrame)
        {
            if (keyState.IsKeyDown(Keys.Escape) || gpState_Player1.IsButtonDown(Buttons.B))
            {
                GameState.currentGameState = GameState.MENU;
            }

            position.Y -= timeSinceLastFrame*100;

            if (position.Y <= LoadedContent.menu_Credits_Additional_Background.Height - LoadedContent.credits.Height)
            {
                position.Y = 480;
            }
        }


        override public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(LoadedContent.menu_Background, Vector2.Zero, Color.White);
            spriteBatch.Draw(LoadedContent.credits, position, Color.White);
            spriteBatch.Draw(LoadedContent.menu_Credits_Additional_Background, new Vector2(0, 0), Color.White);
            spriteBatch.Draw(LoadedContent.menu_Text_ZombieGearSolid, new Vector2(400 - LoadedContent.menu_Text_ZombieGearSolid.Width / 2, 0), Color.White);
            spriteBatch.Draw(LoadedContent.menu_Text_Credits, new Vector2(400 - LoadedContent.menu_Text_Credits.Width / 2, 100 + LoadedContent.menu_Text_Credits.Height), Color.White);

        }
    }
}
