﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZombieGearSolid
{
    public class LoadedContent
    {
        public static Texture2D[,] background = new Texture2D[3,3];

        public static Texture2D pause;

        
        public static Texture2D player_Stop_Up;
        public static Texture2D player_Stop_Down;
        public static Texture2D player_Stop_Left;
        public static Texture2D player_Stop_Right;
        public static Texture2D player_Walk_Left_1;
        public static Texture2D player_Walk_Left_2;
        public static Texture2D player_Walk_Right_1;
        public static Texture2D player_Walk_Right_2;
        public static Texture2D player_Walk_Up_1;
        public static Texture2D player_Walk_Up_2;
        public static Texture2D player_Walk_Down_1;
        public static Texture2D player_Walk_Down_2;
        public static Texture2D player_Dead;
        public static Texture2D player_Fadenkreuz;

        public static Texture2D player2_Stop_Up;
        public static Texture2D player2_Stop_Down;
        public static Texture2D player2_Stop_Left;
        public static Texture2D player2_Stop_Right;
        public static Texture2D player2_Walk_Left_1;
        public static Texture2D player2_Walk_Left_2;
        public static Texture2D player2_Walk_Right_1;
        public static Texture2D player2_Walk_Right_2;
        public static Texture2D player2_Walk_Up_1;
        public static Texture2D player2_Walk_Up_2;
        public static Texture2D player2_Walk_Down_1;
        public static Texture2D player2_Walk_Down_2;
        public static Texture2D player2_Dead;

        public static Texture2D enemy_Stop_Up;
        public static Texture2D enemy_Stop_Down;
        public static Texture2D enemy_Stop_Left;
        public static Texture2D enemy_Stop_Right;
        public static Texture2D enemy_Walk_Left_1;
        public static Texture2D enemy_Walk_Left_2;
        public static Texture2D enemy_Walk_Right_1;
        public static Texture2D enemy_Walk_Right_2;
        public static Texture2D enemy_Walk_Up_1;
        public static Texture2D enemy_Walk_Up_2;
        public static Texture2D enemy_Walk_Down_1;
        public static Texture2D enemy_Walk_Down_2;

        public static Texture2D enemyRandom_Stop_Up;
        public static Texture2D enemyRandom_Stop_Down;
        public static Texture2D enemyRandom_Stop_Left;
        public static Texture2D enemyRandom_Stop_Right;
        public static Texture2D enemyRandom_Walk_Left_1;
        public static Texture2D enemyRandom_Walk_Left_2;
        public static Texture2D enemyRandom_Walk_Right_1;
        public static Texture2D enemyRandom_Walk_Right_2;
        public static Texture2D enemyRandom_Walk_Up_1;
        public static Texture2D enemyRandom_Walk_Up_2;
        public static Texture2D enemyRandom_Walk_Down_1;
        public static Texture2D enemyRandom_Walk_Down_2;

        public static Texture2D weapon_Up;
        public static Texture2D weapon_Down;
        public static Texture2D weapon_Left;
        public static Texture2D weapon_Right;

        /*
         * Small  = (24*24)
         * Normal = (48*48)
         * Big    = (72*72)
         * Large  = (96*96)
         */
        public static Texture2D stone_Small;
        public static Texture2D stone_Normal;
        public static Texture2D stone_Big;
        public static Texture2D stone_Large;
        public static Texture2D box_Small;
        public static Texture2D box_Normal;
        public static Texture2D bush_Normal;
        public static Texture2D bush_Big;

        public static Texture2D bullet_1;
        public static Texture2D bullet_2;
        public static Texture2D bullet_3;
        public static Texture2D bullet_4;

        public static Texture2D shadow;

        public static Texture2D hud_Heart_Full;
        public static Texture2D hud_Heart_Empty;
        public static Texture2D[] hud_Number = new Texture2D[10];
        public static Texture2D hud_Text_Level;
        public static Texture2D hud_Text_Win;
        public static Texture2D hud_Text_Lose;
        public static Texture2D hud_Ammo;

        public static Texture2D menu_Background;
        public static Texture2D menu_Text_ZombieGearSolid;
        public static Texture2D menu_Text_Options;
        public static Texture2D menu_Text_Credits;
        public static Texture2D menu_Text_Highscore;
        public static Texture2D menu_Text_Multiplayer;
        public static Texture2D menu_Text_Singleplayer;
        public static Texture2D menu_Button_Singleplayer_Pressed;
        public static Texture2D menu_Button_Singleplayer_NotPressed;
        public static Texture2D menu_Button_Multiplayer_Pressed;
        public static Texture2D menu_Button_Multiplayer_NotPressed;
        public static Texture2D menu_Button_Options_Pressed;
        public static Texture2D menu_Button_Options_NotPressed;
        public static Texture2D menu_Button_Credits_Pressed;
        public static Texture2D menu_Button_Credits_NotPressed;
        public static Texture2D menu_Button_Highscore_Pressed;
        public static Texture2D menu_Button_Highscore_NotPressed;
        public static Texture2D menu_Button_Zuruck_Pressed;
        public static Texture2D menu_Button_Zuruck_NotPressed;
        public static Texture2D menu_Credits_Additional_Background;
        public static Texture2D menu_Button_Weiter_Pressed;
        public static Texture2D menu_Button_Weiter_NotPressed;
        public static Texture2D menu_Button_Neustart_Pressed;
        public static Texture2D menu_Button_Neustart_NotPressed;
        public static Texture2D menu_Button_Beenden_Pressed;
        public static Texture2D menu_Button_Beenden_NotPressed;

        public static Texture2D menu_Button_Player1_Pressed;
        public static Texture2D menu_Button_Player1_NotPressed;
        public static Texture2D menu_Button_Player2_Pressed;
        public static Texture2D menu_Button_Player2_NotPressed;
        public static Texture2D menu_Button_Keyboard;
        public static Texture2D menu_Button_Gamepad1;
        public static Texture2D menu_Button_Gamepad2;
  

        public static Texture2D credits;


        public static Texture2D reddot;
        public static Texture2D orangedot;
        public static Texture2D dot;

        public static bool gameOver = false;



        public static Vector2 defaultRootPosition = new Vector2(800/2 - 12, 480/2 - 12);

        public static SpriteFont font;


        public static int player1_Controlmode = Player.CONTROLMODE_KEYBOARD;
        public static int player2_Controlmode = Player.CONTROLMODE_GAMEPAD1;

        public static String highscorePath;
        public static HighscoreData highscoreData;
    }
}
