﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZombieGearSolid
{

    public class StateHighscore : GameState
    {
        public StateHighscore(Game1 g1) : base(g1) { }

        override public void update(float timeSinceLastFrame)
        {
            //Highscores wieder verlassen
            if (keyState.IsKeyDown(Keys.Escape) || gpState_Player1.IsButtonDown(Buttons.B))
            {
                GameState.currentGameState = GameState.MENU;
            }
        }


        override public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(LoadedContent.menu_Background, Vector2.Zero, Color.White);
            spriteBatch.Draw(LoadedContent.menu_Text_ZombieGearSolid, new Vector2(400 - LoadedContent.menu_Text_ZombieGearSolid.Width / 2, 0), Color.White);
            spriteBatch.Draw(LoadedContent.menu_Text_Highscore, new Vector2(400 - LoadedContent.menu_Text_Highscore.Width / 2, 100 + LoadedContent.menu_Text_Highscore.Height), Color.White);

            for (int i = LoadedContent.highscoreData.highscores.Length-1; i >= 0; i--)
            {
                spriteBatch.DrawString(LoadedContent.font, "Platz "+(10-i)+": " + LoadedContent.highscoreData.highscores[i], new Vector2(350, 420 - i * 25), Color.White);
            }
        }

    }
}
