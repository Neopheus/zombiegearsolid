﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZombieGearSolid
{
    public class StateMainmenu : GameState
    {
        private const int SELECTED_SINGLEPLAYER = 0;
        private const int SELECTED_MULTIPLAYER = 1;
        private const int SELECTED_OPTIONS = 2;
        private const int SELECTED_HIGHSCORE = 3;
        private const int SELECTED_CREDITS = 4;
        int selected = SELECTED_SINGLEPLAYER;

        float timeSinceLastChange = 0;
        public float timeSinceLastEnter = 0.5f;

        private const float MIN_TIME_BETWEEN_CHANGES = 0.2f;

        public StateMainmenu(Game1 g1) : base(g1) { }
        


        override public void update(float timeSinceLastFrame)
        {
            timeSinceLastChange += timeSinceLastFrame;
            timeSinceLastEnter += timeSinceLastFrame;

            if (keyState.IsKeyUp(Keys.W) && keyState.IsKeyUp(Keys.S) && keyState.IsKeyDown(Keys.Up)&& keyState.IsKeyDown(Keys.Down)&&gpState_Player1.ThumbSticks.Left.Y == 0)
            {
                timeSinceLastChange = MIN_TIME_BETWEEN_CHANGES;
            }

            //Menüsteuerung
            if (timeSinceLastChange > MIN_TIME_BETWEEN_CHANGES)
            {
                if (keyState.IsKeyDown(Keys.W) || keyState.IsKeyDown(Keys.Up) || gpState_Player1.ThumbSticks.Left.Y > 0)
                {
                    timeSinceLastChange = 0;
                    selected--;
                    if (selected < SELECTED_SINGLEPLAYER)
                    {
                        selected = SELECTED_CREDITS;
                    }
                }
                else if (keyState.IsKeyDown(Keys.S) || keyState.IsKeyDown(Keys.Down) || gpState_Player1.ThumbSticks.Left.Y < 0)
                {
                    timeSinceLastChange = 0;
                    selected++;
                    if (selected > SELECTED_CREDITS)
                    {
                        selected = SELECTED_SINGLEPLAYER;
                    }
                }
            }

            if ((timeSinceLastEnter>0.5f)&&(keyState.IsKeyDown(Keys.Enter) || gpState_Player1.IsButtonDown(Buttons.A) || gpState_Player2.IsButtonDown(Buttons.A)))
            {
                switch (selected)
                {
                    case (SELECTED_SINGLEPLAYER):
                        GameState.currentGameState = GameState.SINGLEPLAYER;
                        g1.graphics.PreferredBackBufferWidth = 800;
                        g1.graphics.PreferredBackBufferHeight = 480;
                        g1.graphics.ApplyChanges();
                        ((StateSingleplayer)(g1.GS_SINGLEPLAYER)).player.controlMode = LoadedContent.player1_Controlmode;
                        break;
                    case (SELECTED_MULTIPLAYER):
                        GameState.currentGameState = GameState.MULTIPLAYER;
                        g1.graphics.PreferredBackBufferWidth = 800;
                        g1.graphics.PreferredBackBufferHeight = 960;
                        g1.graphics.ApplyChanges();
                        g1.GS_MULTIPLAYER.player.controlMode = LoadedContent.player1_Controlmode;
                        g1.GS_MULTIPLAYER.player2.controlMode = LoadedContent.player2_Controlmode;
                        break;
                    case (SELECTED_OPTIONS):
                        GameState.currentGameState = GameState.OPTIONS;
                        g1.graphics.PreferredBackBufferWidth = 800;
                        g1.graphics.PreferredBackBufferHeight = 480;
                        g1.graphics.ApplyChanges();
                        break;
                    case (SELECTED_HIGHSCORE):
                        GameState.currentGameState = GameState.HIGHSCORE;
                        g1.graphics.PreferredBackBufferWidth = 800;
                        g1.graphics.PreferredBackBufferHeight = 480;
                        g1.graphics.ApplyChanges();
                        break;
                    case (SELECTED_CREDITS):
                        GameState.currentGameState = GameState.CREDITS;
                        g1.graphics.PreferredBackBufferWidth = 800;
                        g1.graphics.PreferredBackBufferHeight = 480;
                        g1.graphics.ApplyChanges();
                        break;
                }
            }
        }


        override public void draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.Draw(gegner[i].texture, gegner[i].pos - karlPos + normalverschiebung, Color.White);
            spriteBatch.Draw(LoadedContent.menu_Background, Vector2.Zero, Color.White);
            spriteBatch.Draw(LoadedContent.menu_Text_ZombieGearSolid, new Vector2(400-LoadedContent.menu_Text_ZombieGearSolid.Width/2, 0), Color.White);

            Texture2D nextPainting = null;
            if (selected == SELECTED_SINGLEPLAYER) nextPainting = LoadedContent.menu_Button_Singleplayer_Pressed;
            else nextPainting = LoadedContent.menu_Button_Singleplayer_NotPressed;
            spriteBatch.Draw(nextPainting, new Vector2(400 - nextPainting.Width / 2, 200), Color.White);

            if (selected == SELECTED_MULTIPLAYER) nextPainting = LoadedContent.menu_Button_Multiplayer_Pressed;
            else nextPainting = LoadedContent.menu_Button_Multiplayer_NotPressed;
            spriteBatch.Draw(nextPainting, new Vector2(400 - nextPainting.Width / 2, 255), Color.White);

            if (selected == SELECTED_OPTIONS) nextPainting = LoadedContent.menu_Button_Options_Pressed;
            else nextPainting = LoadedContent.menu_Button_Options_NotPressed;
            spriteBatch.Draw(nextPainting, new Vector2(400 - nextPainting.Width / 2, 310), Color.White);

            if (selected == SELECTED_HIGHSCORE) nextPainting = LoadedContent.menu_Button_Highscore_Pressed;
            else nextPainting = LoadedContent.menu_Button_Highscore_NotPressed;
            spriteBatch.Draw(nextPainting, new Vector2(400 - nextPainting.Width / 2, 365), Color.White);

            if (selected == SELECTED_CREDITS) nextPainting = LoadedContent.menu_Button_Credits_Pressed;
            else nextPainting = LoadedContent.menu_Button_Credits_NotPressed;
            spriteBatch.Draw(nextPainting, new Vector2(400 - nextPainting.Width / 2, 420), Color.White);

            //spriteBatch.Draw(LoadedContent.hud_Text_Lose, new Vector2(0, 0), new Rectangle(150, 150, 100, 100), Color.White);
            //spriteBatch.DrawString(LoadedContent.font, "" + gpState_Player1.ThumbSticks.Left.Y, new Vector2(20, 20), Color.White);

        }
    }
}
