﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ZombieGearSolid
{
    public class HighscoreData
    {
        public int[] highscores = new int[10];
        private int niedrigstePunktzahl;
        private StreamWriter streamWriter;

        public HighscoreData()
        {
            String highscorepath = LoadedContent.highscorePath;

            if (!File.Exists(highscorepath))
            {
                streamWriter = new StreamWriter(LoadedContent.highscorePath);
                for (int i = 0; i < highscores.Length; i++)
                {
                    streamWriter.WriteLine(0);
                }
                streamWriter.Close();
                streamWriter.Dispose();
            }

            StreamReader streamReader = File.OpenText(highscorepath);



            for (int i = 0; i < highscores.Length; i++)
            {
                String line = streamReader.ReadLine();
                highscores[i] = Int32.Parse(line);
            }
            Array.Sort(highscores);
            niedrigstePunktzahl = highscores[0];
            streamReader.Close();
        }

        public void neuePunktzahl(int punkte)
        {

            StreamReader streamReader = File.OpenText(LoadedContent.highscorePath);



            for (int i = 0; i < highscores.Length; i++)
            {
                String line = streamReader.ReadLine();
                highscores[i] = Int32.Parse(line);
            }
            Array.Sort(highscores);
            niedrigstePunktzahl = highscores[0];
            streamReader.Close();


            if (punkte > niedrigstePunktzahl)
            {
                highscores[0] = punkte;
                Array.Sort(highscores);
                niedrigstePunktzahl = highscores[0];
                streamWriter = new StreamWriter(LoadedContent.highscorePath);
                for (int i = 0; i < highscores.Length; i++)
                {
                    streamWriter.WriteLine(highscores[i]);
                }
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }
    }
}
