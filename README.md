# README #

Group project as part of the media informatics course at the "Hochschule der Medien" (Media University).

### About the game ###

ZombieGearSolid is a simple 2D topdown shooter in a postapocalyptical world where you have to survive waves of zombies.

### About the implementation ###

It's written in C# and uses the XNA Framework. 