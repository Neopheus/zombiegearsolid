﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZombieGearSolid
{
    public class Player2 : Player
    {
        public new Texture2D look
        {
            get
            {
                switch (direction)
                {
                    case (DIRECTION_UP):
                        if (speed == Vector2.Zero)
                        {
                            return LoadedContent.player2_Stop_Up;
                        }
                        else
                        {
                            if (anitime <= (NEEDEDANITIME / 2))
                            {
                                return LoadedContent.player2_Walk_Up_1;
                            }
                            else return LoadedContent.player2_Walk_Up_2;
                        }
                    case (DIRECTION_DOWN):
                        if (speed == Vector2.Zero)
                        {
                            return LoadedContent.player2_Stop_Down;
                        }
                        else
                        {
                            if (anitime <= (NEEDEDANITIME / 2))
                            {
                                return LoadedContent.player2_Walk_Down_1;
                            }
                            else return LoadedContent.player2_Walk_Down_2;
                        }
                    case (DIRECTION_LEFT):
                        if (speed == Vector2.Zero)
                        {
                            return LoadedContent.player2_Stop_Left;
                        }
                        else
                        {
                            if (anitime <= (NEEDEDANITIME / 2))
                            {
                                return LoadedContent.player2_Walk_Left_1;
                            }
                            else return LoadedContent.player2_Walk_Left_2;
                        }
                    case (DIRECTION_RIGHT):
                        if (speed == Vector2.Zero)
                        {
                            return LoadedContent.player2_Stop_Right;
                        }
                        else
                        {
                            if (anitime <= (NEEDEDANITIME / 2))
                            {
                                return LoadedContent.player2_Walk_Right_1;
                            }
                            else return LoadedContent.player2_Walk_Right_2;
                        }

                    default: return null;
                }
            }
        }




        public Player2(int controlMode, List<Obstacle> obstacles,List<Bullet> bullets, List<EnemyHunter> enemyHunter, List<EnemyRandom> enemyRandom)
            : base(controlMode, obstacles, bullets, enemyHunter, enemyRandom)
        { }
    }
}
