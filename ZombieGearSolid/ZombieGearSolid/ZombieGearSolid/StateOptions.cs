﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZombieGearSolid
{

    public class StateOptions : GameState
    {
        private const int SELECTED_PLAYER1 = 0;
        private const int SELECTED_PLAYER2 = 1;

        int selected = SELECTED_PLAYER1;
        private const float MIN_TIME_BETWEEN_CHANGES = 0.2f;
        private float timeSinceLastChange = 0;

        //Konstruktor : geerbt
        public StateOptions(Game1 g1) : base(g1) { }

        override public void update(float timeSinceLastFrame)
        {
            timeSinceLastChange += timeSinceLastFrame;


            if (keyState.IsKeyUp(Keys.W) && keyState.IsKeyUp(Keys.S) && keyState.IsKeyDown(Keys.Up) && keyState.IsKeyDown(Keys.Down) && gpState_Player1.ThumbSticks.Left.Y == 0)
            {
                timeSinceLastChange = MIN_TIME_BETWEEN_CHANGES;
            }

            if (timeSinceLastChange > MIN_TIME_BETWEEN_CHANGES)
            {
                if (keyState.IsKeyDown(Keys.W) || keyState.IsKeyDown(Keys.S) || keyState.IsKeyDown(Keys.Up) || keyState.IsKeyDown(Keys.Down) || gpState_Player1.ThumbSticks.Left.Y != 0)
                {
                    timeSinceLastChange = 0;

                    if (keyState.IsKeyDown(Keys.W) || keyState.IsKeyDown(Keys.Up) || gpState_Player1.ThumbSticks.Left.Y < -0.1)
                    {
                        selected--;
                        if (selected < 0) selected = SELECTED_PLAYER2;
                    }

                    if (keyState.IsKeyDown(Keys.S) || keyState.IsKeyDown(Keys.Down) || gpState_Player1.ThumbSticks.Left.Y > 0.1)
                    {
                        selected--;
                        if (selected < 0) selected = SELECTED_PLAYER2;
                    }
                }
            }



            if (selected == SELECTED_PLAYER1)
            {
                if (keyState.IsKeyDown(Keys.Enter)) LoadedContent.player1_Controlmode = Player.CONTROLMODE_KEYBOARD;
                else if (gpState_Player1.Buttons.A == ButtonState.Pressed) LoadedContent.player1_Controlmode = Player.CONTROLMODE_GAMEPAD1;
                else if (gpState_Player2.Buttons.A == ButtonState.Pressed) LoadedContent.player1_Controlmode = Player.CONTROLMODE_GAMEPAD2;
            }
            else if (selected == SELECTED_PLAYER2)
            {
                if (keyState.IsKeyDown(Keys.Enter)) LoadedContent.player2_Controlmode = Player.CONTROLMODE_KEYBOARD;
                else if (gpState_Player1.Buttons.A == ButtonState.Pressed) LoadedContent.player2_Controlmode = Player.CONTROLMODE_GAMEPAD1;
                else if (gpState_Player2.Buttons.A == ButtonState.Pressed) LoadedContent.player2_Controlmode = Player.CONTROLMODE_GAMEPAD2;
            }
            

            //Options wieder verlassen
            if (keyState.IsKeyDown(Keys.Escape) || gpState_Player1.IsButtonDown(Buttons.B))
            {
                GameState.currentGameState = GameState.MENU;
            }
        }


        override public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(LoadedContent.menu_Background, Vector2.Zero, Color.White);
            spriteBatch.Draw(LoadedContent.menu_Text_ZombieGearSolid, new Vector2(400 - LoadedContent.menu_Text_ZombieGearSolid.Width / 2, 0), Color.White);
            spriteBatch.Draw(LoadedContent.menu_Text_Options, new Vector2(400 - LoadedContent.menu_Text_Options.Width / 2, 100 + LoadedContent.menu_Text_Options.Height), Color.White);

            Texture2D nextPainting = null;
            if (selected == SELECTED_PLAYER1) nextPainting = LoadedContent.menu_Button_Player1_Pressed;
            else nextPainting = LoadedContent.menu_Button_Player1_NotPressed;
            spriteBatch.Draw(nextPainting, new Vector2(400 - nextPainting.Width / 2, 250), Color.White);

            if (selected == SELECTED_PLAYER2) nextPainting = LoadedContent.menu_Button_Player2_Pressed;
            else nextPainting = LoadedContent.menu_Button_Player2_NotPressed;
            spriteBatch.Draw(nextPainting, new Vector2(400 - nextPainting.Width / 2, 305), Color.White);

            if (LoadedContent.player1_Controlmode == Player.CONTROLMODE_KEYBOARD) nextPainting = LoadedContent.menu_Button_Keyboard;
            else if (LoadedContent.player1_Controlmode == Player.CONTROLMODE_GAMEPAD1) nextPainting = LoadedContent.menu_Button_Gamepad1;
            else if (LoadedContent.player1_Controlmode == Player.CONTROLMODE_GAMEPAD2) nextPainting = LoadedContent.menu_Button_Gamepad2;
            spriteBatch.Draw(nextPainting, new Vector2(600, 250), Color.White);

            if (LoadedContent.player2_Controlmode == Player.CONTROLMODE_KEYBOARD) nextPainting = LoadedContent.menu_Button_Keyboard;
            else if (LoadedContent.player2_Controlmode == Player.CONTROLMODE_GAMEPAD1) nextPainting = LoadedContent.menu_Button_Gamepad1;
            else if (LoadedContent.player2_Controlmode == Player.CONTROLMODE_GAMEPAD2) nextPainting = LoadedContent.menu_Button_Gamepad2;
            spriteBatch.Draw(nextPainting, new Vector2(600, 305), Color.White);


            spriteBatch.DrawString(LoadedContent.font, "Wähle einen Spieler aus und drücke dann Enter (Tastatur) oder A (Gamepad)", new Vector2(0, 425), Color.White);
            spriteBatch.DrawString(LoadedContent.font, "um die Steurung umzustellen. ESC oder B -> Hauptmenü", new Vector2(0, 450), Color.White);
        }
    }
}
