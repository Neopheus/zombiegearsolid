﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ZombieGearSolid
{
    public class Player
    {
        public const int FULLAMMO = 25;
        public const int FULLHP = 10;
        protected const float RELOADTIME = 1.0f;      //0.6f
        protected const float SHOTINTERVALL = 0.1f;   //0.2f
        protected const float NEEDEDANITIME = 0.5f;
        protected const int DIRECTION_UP = 0;
        protected const int DIRECTION_DOWN = 1;
        protected const int DIRECTION_LEFT = 2;
        protected const int DIRECTION_RIGHT = 3;
        protected const int BASICSPEED = 300;         //200
        public const int CONTROLMODE_KEYBOARD = 0;
        public const int CONTROLMODE_GAMEPAD1 = 1;
        public const int CONTROLMODE_GAMEPAD2 = 2;
        protected Vector2 BULLETSPAWN_UP = new Vector2(15, 0);
        protected Vector2 BULLETSPAWN_DOWN = new Vector2(1, 17);
        protected Vector2 BULLETSPAWN_LEFT = new Vector2(-1, 2);
        protected Vector2 BULLETSPAWN_RIGHT = new Vector2(16, 15);
        public Vector2 STARTPOSITION = new Vector2(2400, 2400);

        public int controlMode = CONTROLMODE_GAMEPAD1;
        public Rectangle boundingBox;
        public int hp = FULLHP;
        public int ammo = FULLAMMO;
        protected int direction;
        protected int weapondirection;
        public Vector2 speed = new Vector2(0, 0);
        public Vector2 prePosition = new Vector2(0, 0);
        public Vector2 position = new Vector2(0, 0);
        public Vector2 maus = new Vector2(0, 0);
        public Vector2 schussVector = new Vector2(0, 0);
        public Vector2 schussSpeed = new Vector2(0, 0);
        public Vector2 schussPosition = new Vector2(0, 0);
        protected float anitime = 0;
        protected float timeSinceLastShot = 0;
        protected float timeSinceLastReloadStart = 0;

        protected List<Obstacle> obstacles;
        protected List<Bullet> bullets;
        protected List<EnemyHunter> enemyHunter;
        protected List<EnemyRandom> enemyRandom;


        //  Aussehen von Spieler und Waffe

        public Texture2D look
        {
            get
            {
                switch (direction)
                {
                    case (DIRECTION_UP):
                        if (speed == Vector2.Zero)
                        {
                            return LoadedContent.player_Stop_Up;
                        }
                        else
                        {
                            if (anitime <= (NEEDEDANITIME / 2))
                            {
                                return LoadedContent.player_Walk_Up_1;
                            }
                            else return LoadedContent.player_Walk_Up_2;
                        }
                    case (DIRECTION_DOWN):
                        if (speed == Vector2.Zero)
                        {
                            return LoadedContent.player_Stop_Down;
                        }
                        else
                        {
                            if (anitime <= (NEEDEDANITIME / 2))
                            {
                                return LoadedContent.player_Walk_Down_1;
                            }
                            else return LoadedContent.player_Walk_Down_2;
                        }
                    case (DIRECTION_LEFT):
                        if (speed == Vector2.Zero)
                        {
                            return LoadedContent.player_Stop_Left;
                        }
                        else
                        {
                            if (anitime <= (NEEDEDANITIME / 2))
                            {
                                return LoadedContent.player_Walk_Left_1;
                            }
                            else return LoadedContent.player_Walk_Left_2;
                        }
                    case (DIRECTION_RIGHT):
                        if (speed == Vector2.Zero)
                        {
                            return LoadedContent.player_Stop_Right;
                        }
                        else
                        {
                            if (anitime <= (NEEDEDANITIME / 2))
                            {
                                return LoadedContent.player_Walk_Right_1;
                            }
                            else return LoadedContent.player_Walk_Right_2;
                        }

                    default: return null;
                }
            }
        }

        public Texture2D weaponlook
        {
            get
            {
                switch (weapondirection)
                {
                    case (DIRECTION_UP):
                        return LoadedContent.weapon_Up;
                    case (DIRECTION_DOWN):
                        return LoadedContent.weapon_Down;
                    case (DIRECTION_LEFT):
                        return LoadedContent.weapon_Left;
                    case (DIRECTION_RIGHT):
                        return LoadedContent.weapon_Right;
                    default: return null;
                }
            }
        }


        // Konstruktor

        public Player(int controlMode, List<Obstacle> obstacles, List<Bullet> bullets, List<EnemyHunter> enemyHunter, List<EnemyRandom> enemyRandom)
        {
            position = STARTPOSITION;
            prePosition = STARTPOSITION;
            this.controlMode = controlMode;
            this.bullets = bullets;
            boundingBox = new Rectangle((int)position.X, (int)position.Y, look.Width, look.Height);
            this.enemyHunter = enemyHunter;
            this.enemyRandom = enemyRandom;
            this.obstacles = obstacles;
        }


        // Updatemethode

        public void Update(float timeSinceLastFrame, KeyboardState keyState, GamePadState gpState1, GamePadState gpState2, MouseState mouseState)
        {
            if (hp <= 0) // Spieler tot
            {
                return;
            }

            GamePadState gpState = gpState1;

            if (controlMode == CONTROLMODE_GAMEPAD1) gpState = gpState1;
            else if (controlMode == CONTROLMODE_GAMEPAD2) gpState = gpState2;

            timeSinceLastReloadStart += timeSinceLastFrame;
            timeSinceLastShot += timeSinceLastFrame;
            anitime += timeSinceLastFrame;

            bool isShooting = false;
            bool sprinting = false;


            // Nachladen

            if (ammo == 0 && timeSinceLastReloadStart > RELOADTIME)
            {
                ammo = FULLAMMO;
            }

            if (anitime > NEEDEDANITIME) anitime = 0;


            // Steuerung mit Tastatur und Maus

            if (keyState != null && controlMode == CONTROLMODE_KEYBOARD)
            {
                speed.X = 0;
                speed.Y = 0;
                sprinting = keyState.IsKeyDown(Keys.LeftShift);
                isShooting = ((mouseState.LeftButton == ButtonState.Pressed) || (keyState.IsKeyDown(Keys.Space)));

                if (keyState.IsKeyDown(Keys.W))
                {
                    speed.Y -= BASICSPEED;
                }
                if (keyState.IsKeyDown(Keys.A))
                {
                    speed.X -= BASICSPEED;
                }
                if (keyState.IsKeyDown(Keys.S))
                {
                    speed.Y += BASICSPEED;
                }
                if (keyState.IsKeyDown(Keys.D))
                {
                    speed.X += BASICSPEED;
                }

                if (speed.X != 0 || speed.Y != 0)
                {
                    speed = Vector2.Normalize(speed) * BASICSPEED;
                }

                //Manuelles Nachladen
                if (keyState.IsKeyDown(Keys.R) || mouseState.RightButton == ButtonState.Pressed)
                {
                    ammo = 0;
                    timeSinceLastReloadStart = 0;
                }

                if (sprinting)
                {
                    speed *= 2f;  //*1.5f
                }


                maus.X = mouseState.X;
                maus.Y = mouseState.Y;

                schussPosition = position;
                schussVector = maus - LoadedContent.defaultRootPosition;

                float betragSchussX = schussVector.X;
                float betragSchussY = schussVector.Y;
                if (betragSchussX < 0) betragSchussX *= -1;
                if (betragSchussY < 0) betragSchussY *= -1;

                if (betragSchussX > betragSchussY)
                {
                    if (schussVector.X < 0) weapondirection = DIRECTION_LEFT;
                    else weapondirection = DIRECTION_RIGHT;
                }
                else if (betragSchussY > betragSchussX)
                {
                    if (schussVector.Y < 0) weapondirection = DIRECTION_UP;
                    else weapondirection = DIRECTION_DOWN;
                }

                if (isShooting && ammo > 0 && !sprinting)
                {
                    if (timeSinceLastShot > SHOTINTERVALL)
                    {
                        timeSinceLastShot = 0;

                        switch (weapondirection)
                        {
                            case (DIRECTION_DOWN):
                                schussPosition += BULLETSPAWN_DOWN;
                                schussVector = maus - (LoadedContent.defaultRootPosition + BULLETSPAWN_DOWN);
                                break;
                            case (DIRECTION_UP):
                                schussPosition += BULLETSPAWN_UP;
                                schussVector = maus - (LoadedContent.defaultRootPosition + BULLETSPAWN_UP);
                                break;
                            case (DIRECTION_LEFT):
                                schussPosition += BULLETSPAWN_LEFT;
                                schussVector = maus - (LoadedContent.defaultRootPosition + BULLETSPAWN_LEFT);
                                break;
                            case (DIRECTION_RIGHT):
                                schussPosition += BULLETSPAWN_RIGHT;
                                schussVector = maus - (LoadedContent.defaultRootPosition + BULLETSPAWN_RIGHT);
                                break;
                        }

                        if (schussVector.Length() != 0)
                        {
                            schussSpeed = Vector2.Normalize(schussVector) * Bullet.BASICSPEED + speed;
                            bullets.Add(new Bullet(schussPosition, schussSpeed, bullets, obstacles));
                            ammo--;
                        }

                        if (ammo == 0)
                        {
                            timeSinceLastReloadStart = 0;
                        }
                    }
                }
            }


            // Steuerung mit Gamepad

            else if (gpState != null && (controlMode == CONTROLMODE_GAMEPAD1 || controlMode == CONTROLMODE_GAMEPAD2))
            {
                isShooting = (gpState.Triggers.Right > 0);
                sprinting = (gpState.Triggers.Left > 0);

                speed = gpState.ThumbSticks.Left * BASICSPEED;

                schussPosition = position;
                schussVector = gpState.ThumbSticks.Right;

                float betragSchussX = schussVector.X;
                float betragSchussY = schussVector.Y;
                if (betragSchussX < 0) betragSchussX *= -1;
                if (betragSchussY < 0) betragSchussY *= -1;

                if (betragSchussX > betragSchussY)
                {
                    if (schussVector.X < 0) weapondirection = DIRECTION_LEFT;
                    else weapondirection = DIRECTION_RIGHT;
                }
                else if (betragSchussY > betragSchussX)
                {
                    if (schussVector.Y < 0) weapondirection = DIRECTION_DOWN;
                    else weapondirection = DIRECTION_UP;
                }

                if (gpState.Buttons.RightShoulder == ButtonState.Pressed)
                {
                    ammo = 0;
                    timeSinceLastReloadStart = 0;
                }

                if (sprinting)
                {
                    speed *= 2f * gpState.Triggers.Left;  //*1.5
                }

                if (isShooting && ammo > 0 && !sprinting && (schussVector.Length() != 0))
                {
                    if (timeSinceLastShot > SHOTINTERVALL)
                    {
                        timeSinceLastShot = 0;

                        switch (weapondirection)
                        {
                            case (DIRECTION_DOWN):
                                schussPosition += BULLETSPAWN_DOWN;
                                break;
                            case (DIRECTION_UP):
                                schussPosition += BULLETSPAWN_UP;
                                break;
                            case (DIRECTION_LEFT):
                                schussPosition += BULLETSPAWN_LEFT;
                                break;
                            case (DIRECTION_RIGHT):
                                schussPosition += BULLETSPAWN_RIGHT;
                                break;

                        }
                        schussSpeed = Vector2.Normalize(gpState.ThumbSticks.Right) * Bullet.BASICSPEED + speed;
                        schussSpeed.Y *= -1;

                        bullets.Add(new Bullet(schussPosition, schussSpeed, bullets, obstacles));
                        ammo--;

                        if (ammo == 0)
                        {
                            timeSinceLastReloadStart = 0;
                        }
                    }
                }
            }


            if (controlMode != CONTROLMODE_KEYBOARD)
            {
                speed.Y *= -1;
            }

            position.X += speed.X * timeSinceLastFrame;
            position.Y += speed.Y * timeSinceLastFrame;


            // Spielfeldbegrenzung

            if (position.X < 400) position.X = 400;
            if (position.Y < 400) position.Y = 400;

            if (position.X > 4400 - 24) position.X = 4400 - 24;
            if (position.Y > 4400 - 24) position.Y = 4400 - 24;


            // Spielerausrichtung

            float betragSpeedX = speed.X;
            float betragSpeedY = speed.Y;
            if (betragSpeedX < 0) betragSpeedX *= -1;
            if (betragSpeedY < 0) betragSpeedY *= -1;

            if (betragSpeedX > betragSpeedY)
            {
                if (speed.X < 0) direction = DIRECTION_LEFT;
                else direction = DIRECTION_RIGHT;
            }
            else if (betragSpeedY > betragSpeedX)
            {
                if (speed.Y < 0) direction = DIRECTION_UP;
                else direction = DIRECTION_DOWN;
            }


            // Kollisionserkennung Gegner

            boundingBox.X = (int)position.X;
            boundingBox.Y = (int)position.Y;

            for (int i = 0; i < enemyRandom.Count; i++)
            {
                if (boundingBox.Intersects(enemyRandom[i].boundingBox) && hp > 0)
                {
                    hp--;
                    enemyRandom.Remove(enemyRandom[i]);
                }
            }

            for (int i = 0; i < enemyHunter.Count; i++)
            {
                if (boundingBox.Intersects(enemyHunter[i].boundingBox) && hp > 0)
                {
                    hp--;
                    enemyHunter.Remove(enemyHunter[i]);
                }
            }


            // Kollisionserkennung Gegenstände

            bool changed = true;
            while (changed)
            {
                changed = false;
                for (int i = 0; i < obstacles.Count; i++)
                {
                    if (boundingBox.Intersects(obstacles[i].boundingBox))
                    {
                        changed = true;
                        //LEFT
                        if (prePosition.X < obstacles[i].position.X && prePosition.Y < obstacles[i].position.Y + obstacles[i].boundingBox.Height && prePosition.X - obstacles[i].position.X < prePosition.Y - obstacles[i].position.Y)
                        {
                            position.X = obstacles[i].position.X - 24;
                            speed.X = 0;
                            boundingBox = new Rectangle((int)position.X, (int)position.Y, 24, 24);
                        }
                        //UP
                        else if (prePosition.Y < obstacles[i].position.Y && prePosition.X < obstacles[i].position.X + obstacles[i].boundingBox.Width)
                        {
                            position.Y = obstacles[i].position.Y - 24;
                            speed.Y = 0;
                            boundingBox = new Rectangle((int)position.X, (int)position.Y, 24, 24);
                        }
                        //RIGHT
                        else if (prePosition.X >= obstacles[i].position.X + obstacles[i].boundingBox.Width)
                        {
                            position.X = obstacles[i].position.X + obstacles[i].boundingBox.Width;
                            speed.X = 0;
                            boundingBox = new Rectangle((int)position.X, (int)position.Y, 24, 24);
                        }
                        //DOWN
                        else
                        {
                            position.Y = obstacles[i].position.Y + obstacles[i].boundingBox.Height;
                            speed.Y = 0;
                            boundingBox = new Rectangle((int)position.X, (int)position.Y, 24, 24);
                        }
                    }
                }
            }


            // MUSS ANS ENDE!!!

            prePosition = position;

        }
    }
}

