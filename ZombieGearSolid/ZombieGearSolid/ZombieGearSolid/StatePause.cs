﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZombieGearSolid
{

    public class StatePause : GameState
    {
        public const int CURRENTSINGLE = 0;
        public const int CURRENTMULTI = 1;
        public int currentState = CURRENTSINGLE;
        private const int SELECTED_WEITER = 0;
        private const int SELECTED_NEUSTART = 1;
        private const int SELECTED_BEENDEN = 2;
        int selected = SELECTED_WEITER;

        float timeSinceLastChange = 0;

        private const float MIN_TIME_BETWEEN_CHANGES = 0.2f;

        StateSingleplayer GS_SINGLEPLAYER;
        StateMultiplayer GS_MULTIPLAYER;

 

        public StatePause(Game1 g1, StateSingleplayer GS_SINGLEPLAYER, StateMultiplayer GS_MULTIPLAYER) : base(g1) {
            this.GS_SINGLEPLAYER = GS_SINGLEPLAYER;
            this.GS_MULTIPLAYER = GS_MULTIPLAYER;
        }


        override public void update(float timeSinceLastFrame)
        {

            timeSinceLastChange += timeSinceLastFrame;

            if (keyState.IsKeyUp(Keys.W) && keyState.IsKeyUp(Keys.S) && keyState.IsKeyUp(Keys.Up) && keyState.IsKeyUp(Keys.Down) && gpState_Player1.ThumbSticks.Left.Y == 0)
            {
                timeSinceLastChange = MIN_TIME_BETWEEN_CHANGES;
            }

            //Menüsteuerung
            if (timeSinceLastChange > MIN_TIME_BETWEEN_CHANGES)
            {
                if (keyState.IsKeyDown(Keys.W) || keyState.IsKeyDown(Keys.Up) || gpState_Player1.ThumbSticks.Left.Y > 0)
                {
                    timeSinceLastChange = 0;
                    selected--;
                    if (selected < SELECTED_WEITER)
                    {
                        selected = SELECTED_BEENDEN;
                    }
                }
                else if (keyState.IsKeyDown(Keys.S) || keyState.IsKeyDown(Keys.Down) || gpState_Player1.ThumbSticks.Left.Y < 0)
                {
                    timeSinceLastChange = 0;
                    selected++;
                    if (selected > SELECTED_BEENDEN)
                    {
                        selected = SELECTED_WEITER;
                    }
                }
            }

            if (keyState.IsKeyDown(Keys.Enter) || gpState_Player1.IsButtonDown(Buttons.A))
            {
                switch (selected)
                {
                    case (SELECTED_WEITER):
                        if(currentState == CURRENTSINGLE)
                        {
                        GameState.currentGameState = GameState.SINGLEPLAYER;
                        g1.graphics.PreferredBackBufferWidth = 800;
                        g1.graphics.PreferredBackBufferHeight = 480;
                        g1.graphics.ApplyChanges();
                        }
                        else if (currentState == CURRENTMULTI)
                        {
                        GameState.currentGameState = GameState.MULTIPLAYER;
                        g1.graphics.PreferredBackBufferWidth = 800;
                        g1.graphics.PreferredBackBufferHeight = 960;
                        g1.graphics.ApplyChanges();
                        }
                        break;
                    case (SELECTED_NEUSTART):
                        if(currentState == CURRENTSINGLE)
                        {
                        GS_SINGLEPLAYER.restart();
                        GameState.currentGameState = GameState.SINGLEPLAYER;
                        g1.graphics.PreferredBackBufferWidth = 800;
                        g1.graphics.PreferredBackBufferHeight = 480;
                        g1.graphics.ApplyChanges();
                        }
                        else if (currentState == CURRENTMULTI)
                        {
                        GS_MULTIPLAYER.restart();
                        GameState.currentGameState = GameState.SINGLEPLAYER;
                        g1.graphics.PreferredBackBufferWidth = 800;
                        g1.graphics.PreferredBackBufferHeight = 960;
                        g1.graphics.ApplyChanges();
                        }
                        break;
                    case (SELECTED_BEENDEN):
                        GS_SINGLEPLAYER.restart();
                        ((StateMainmenu)g1.GS_MAINMENU).timeSinceLastEnter = 0;
                        GameState.currentGameState = GameState.MENU;
                        g1.graphics.PreferredBackBufferWidth = 800;
                        g1.graphics.PreferredBackBufferHeight = 480;
                        g1.graphics.ApplyChanges();
                        break;
                }
            }
        }


        override public void draw(SpriteBatch spriteBatch)
        {
            if(currentState == CURRENTSINGLE) g1.GS_SINGLEPLAYER.draw(spriteBatch);
            else if(currentState == CURRENTMULTI) g1.GS_MULTIPLAYER.draw(spriteBatch);
            spriteBatch.Draw(LoadedContent.pause, Vector2.Zero, Color.White);

	        Texture2D nextPainting = null;
            if (selected == SELECTED_WEITER) nextPainting = LoadedContent.menu_Button_Weiter_Pressed;
            else nextPainting = LoadedContent.menu_Button_Weiter_NotPressed;
            spriteBatch.Draw(nextPainting, new Vector2(400 - nextPainting.Width / 2, 200), Color.White);

            if (selected == SELECTED_NEUSTART) nextPainting = LoadedContent.menu_Button_Neustart_Pressed;
            else nextPainting = LoadedContent.menu_Button_Neustart_NotPressed;
            spriteBatch.Draw(nextPainting, new Vector2(400 - nextPainting.Width / 2, 255), Color.White);

            if (selected == SELECTED_BEENDEN) nextPainting = LoadedContent.menu_Button_Beenden_Pressed;
            else nextPainting = LoadedContent.menu_Button_Beenden_NotPressed;
            spriteBatch.Draw(nextPainting, new Vector2(400 - nextPainting.Width / 2, 310), Color.White);

        }
    }
}
