﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ZombieGearSolid
{
    public abstract class GameState
    {
        public static int currentGameState = MENU;
        public const int MENU                = 0;
        public const int SINGLEPLAYER        = 1;
        public const int MULTIPLAYER         = 2;
        public const int OPTIONS             = 3;
        public const int CREDITS             = 4;
        public const int HIGHSCORE           = 5;
        public const int PAUSE               = 6;

        protected Game1 g1;
        public static KeyboardState keyState;
        public static MouseState mouseState;
        public static GamePadState gpState_Player1;
        public static GamePadState gpState_Player2;

        public GameState(Game1 g1)
        {
            this.g1 = g1;
        }
        public abstract void update(float timeSinceLastFrame);
        public abstract void draw(SpriteBatch spriteBatch);
    }
}
