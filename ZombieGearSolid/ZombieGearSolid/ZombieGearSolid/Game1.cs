using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ZombieGearSolid
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;


        public StateMainmenu GS_MAINMENU;
        public StateSingleplayer GS_SINGLEPLAYER;
        public StateMultiplayer GS_MULTIPLAYER;
        public StateOptions GS_OPTIONS;
        public StateCredits GS_CREDITS;
        public StateHighscore GS_HIGHSCORE;
        public StatePause GS_PAUSE;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            LoadedContent.background[0, 0] = Content.Load<Texture2D>("Background_0-0");
            LoadedContent.background[0, 1] = Content.Load<Texture2D>("Background_0-1"); ;
            LoadedContent.background[0, 2] = Content.Load<Texture2D>("Background_0-2"); ;
            LoadedContent.background[1, 0] = Content.Load<Texture2D>("Background_1-0"); ;
            LoadedContent.background[1, 1] = Content.Load<Texture2D>("Background_1-1"); ;
            LoadedContent.background[1, 2] = Content.Load<Texture2D>("Background_1-2"); ;
            LoadedContent.background[2, 0] = Content.Load<Texture2D>("Background_2-0"); ;
            LoadedContent.background[2, 1] = Content.Load<Texture2D>("Background_2-1"); ;
            LoadedContent.background[2, 2] = Content.Load<Texture2D>("Background_2-2"); ;


            LoadedContent.player_Stop_Up = Content.Load<Texture2D>("Player_Up");
            LoadedContent.player_Stop_Down = Content.Load<Texture2D>("Player_Down");
            LoadedContent.player_Stop_Left = Content.Load<Texture2D>("Player_Left");
            LoadedContent.player_Stop_Right = Content.Load<Texture2D>("Player_Right");
            LoadedContent.player_Walk_Left_1 = Content.Load<Texture2D>("Player_Left_1");
            LoadedContent.player_Walk_Left_2 = Content.Load<Texture2D>("Player_Left_2");
            LoadedContent.player_Walk_Right_1 = Content.Load<Texture2D>("Player_Right_1");
            LoadedContent.player_Walk_Right_2 = Content.Load<Texture2D>("Player_Right_2");
            LoadedContent.player_Walk_Up_1 = Content.Load<Texture2D>("Player_Up_1");
            LoadedContent.player_Walk_Up_2 = Content.Load<Texture2D>("Player_Up_2");
            LoadedContent.player_Walk_Down_1 = Content.Load<Texture2D>("Player_Down_1");
            LoadedContent.player_Walk_Down_2 = Content.Load<Texture2D>("Player_Down_2");
            LoadedContent.player_Dead = Content.Load<Texture2D>("Player_Dead");
            LoadedContent.player_Fadenkreuz = Content.Load<Texture2D>("Fadenkreuz");

            LoadedContent.player2_Stop_Up = null;
            LoadedContent.player2_Stop_Down = null;
            LoadedContent.player2_Stop_Left = null;
            LoadedContent.player2_Stop_Right = null;
            LoadedContent.player2_Walk_Left_1 = null;
            LoadedContent.player2_Walk_Left_2 = null;
            LoadedContent.player2_Walk_Right_1 = null;
            LoadedContent.player2_Walk_Right_2 = null;
            LoadedContent.player2_Walk_Up_1 = null;
            LoadedContent.player2_Walk_Up_2 = null;
            LoadedContent.player2_Walk_Down_1 = null;
            LoadedContent.player2_Walk_Down_2 = null;
            LoadedContent.player2_Dead = null;

            LoadedContent.pause = Content.Load<Texture2D>("Pause_Background");

            LoadedContent.enemy_Stop_Up = Content.Load<Texture2D>("Enemy_Up");
            LoadedContent.enemy_Stop_Down = Content.Load<Texture2D>("Enemy_Down");
            LoadedContent.enemy_Stop_Left = Content.Load<Texture2D>("Enemy_Left");
            LoadedContent.enemy_Stop_Right = Content.Load<Texture2D>("Enemy_Right");
            LoadedContent.enemy_Walk_Left_1 = Content.Load<Texture2D>("Enemy_Left_1");
            LoadedContent.enemy_Walk_Left_2 = Content.Load<Texture2D>("Enemy_Left_2");
            LoadedContent.enemy_Walk_Right_1 = Content.Load<Texture2D>("Enemy_Right_1");
            LoadedContent.enemy_Walk_Right_2 = Content.Load<Texture2D>("Enemy_Right_2");
            LoadedContent.enemy_Walk_Up_1 = Content.Load<Texture2D>("Enemy_Up_1");
            LoadedContent.enemy_Walk_Up_2 = Content.Load<Texture2D>("Enemy_Up_2");
            LoadedContent.enemy_Walk_Down_1 = Content.Load<Texture2D>("Enemy_Down_1");
            LoadedContent.enemy_Walk_Down_2 = Content.Load<Texture2D>("Enemy_Down_2");


            LoadedContent.enemyRandom_Stop_Up = Content.Load<Texture2D>("EnemyRandom_Up");
            LoadedContent.enemyRandom_Stop_Down = Content.Load<Texture2D>("EnemyRandom_Down");
            LoadedContent.enemyRandom_Stop_Left = Content.Load<Texture2D>("EnemyRandom_Left");
            LoadedContent.enemyRandom_Stop_Right = Content.Load<Texture2D>("EnemyRandom_Right");
            LoadedContent.enemyRandom_Walk_Left_1 = Content.Load<Texture2D>("EnemyRandom_Left_1");
            LoadedContent.enemyRandom_Walk_Left_2 = Content.Load<Texture2D>("EnemyRandom_Left_2");
            LoadedContent.enemyRandom_Walk_Right_1 = Content.Load<Texture2D>("EnemyRandom_Right_1");
            LoadedContent.enemyRandom_Walk_Right_2 = Content.Load<Texture2D>("EnemyRandom_Right_2");
            LoadedContent.enemyRandom_Walk_Up_1 = Content.Load<Texture2D>("EnemyRandom_Up_1");
            LoadedContent.enemyRandom_Walk_Up_2 = Content.Load<Texture2D>("EnemyRandom_Up_2");
            LoadedContent.enemyRandom_Walk_Down_1 = Content.Load<Texture2D>("EnemyRandom_Down_1");
            LoadedContent.enemyRandom_Walk_Down_2 = Content.Load<Texture2D>("EnemyRandom_Down_2");

            LoadedContent.weapon_Up = Content.Load<Texture2D>("Weapon_Up");
            LoadedContent.weapon_Down = Content.Load<Texture2D>("Weapon_Down");
            LoadedContent.weapon_Left = Content.Load<Texture2D>("Weapon_Left");
            LoadedContent.weapon_Right = Content.Load<Texture2D>("Weapon_Right");

            /*
             * Small  = (24*24)
             * Normal = (48*48)
             * Big    = (72*72)
             * Large  = (96*96)
             */
            LoadedContent.stone_Small = Content.Load<Texture2D>("Rock_24x24");
            LoadedContent.stone_Normal = Content.Load<Texture2D>("Rock_48x48");
            LoadedContent.stone_Big = Content.Load<Texture2D>("Rock_72x72");
            LoadedContent.stone_Large = Content.Load<Texture2D>("Rock_96x96");
            LoadedContent.box_Small = Content.Load<Texture2D>("Box_24x24");
            LoadedContent.box_Normal = Content.Load<Texture2D>("Box_48x48");
            LoadedContent.bush_Normal = Content.Load<Texture2D>("Bush_48x48");
            LoadedContent.bush_Big = Content.Load<Texture2D>("Bush_72x72");

            LoadedContent.bullet_1 = Content.Load<Texture2D>("Bullet_1");
            LoadedContent.bullet_2 = Content.Load<Texture2D>("Bullet_2");
            LoadedContent.bullet_3 = Content.Load<Texture2D>("Bullet_3");
            LoadedContent.bullet_4 = Content.Load<Texture2D>("Bullet_4");

            LoadedContent.shadow = Content.Load<Texture2D>("Shadow");

            LoadedContent.hud_Heart_Full = Content.Load<Texture2D>("Herz");
            LoadedContent.hud_Heart_Empty = Content.Load<Texture2D>("Herzleer");
            LoadedContent.hud_Number[0] = null;
            LoadedContent.hud_Number[1] = null;
            LoadedContent.hud_Number[2] = null;
            LoadedContent.hud_Number[3] = null;
            LoadedContent.hud_Number[4] = null;
            LoadedContent.hud_Number[5] = null;
            LoadedContent.hud_Number[6] = null;
            LoadedContent.hud_Number[7] = null;
            LoadedContent.hud_Number[8] = null;
            LoadedContent.hud_Number[9] = null;

            LoadedContent.hud_Text_Level = null;
            LoadedContent.hud_Text_Win = Content.Load<Texture2D>("Cpmpleted"); ;
            LoadedContent.hud_Text_Lose = Content.Load<Texture2D>("You_Lose");
            LoadedContent.hud_Ammo = Content.Load<Texture2D>("Schrot");

            LoadedContent.menu_Background = Content.Load<Texture2D>("Menu_Background");
            LoadedContent.menu_Text_ZombieGearSolid = Content.Load<Texture2D>("Title");
            LoadedContent.menu_Text_Options = Content.Load<Texture2D>("Menu_Optionen");
            LoadedContent.menu_Text_Credits = Content.Load<Texture2D>("Menu_Credits");
            LoadedContent.menu_Text_Highscore = Content.Load<Texture2D>("Menu_Highscore");
            LoadedContent.menu_Text_Multiplayer = Content.Load<Texture2D>("Menu_Multiplayer");
            LoadedContent.menu_Text_Singleplayer = Content.Load<Texture2D>("Menu_Singleplayer");
            LoadedContent.menu_Button_Singleplayer_Pressed = Content.Load<Texture2D>("Button_Singleplayer_Pressed");
            LoadedContent.menu_Button_Singleplayer_NotPressed = Content.Load<Texture2D>("Button_Singleplayer_Unpressed");
            LoadedContent.menu_Button_Multiplayer_Pressed = Content.Load<Texture2D>("Button_Multiplayer_Pressed");
            LoadedContent.menu_Button_Multiplayer_NotPressed = Content.Load<Texture2D>("Button_Multiplayer_Unpressed");
            LoadedContent.menu_Button_Options_Pressed = Content.Load<Texture2D>("Button_Options_Pressed");
            LoadedContent.menu_Button_Options_NotPressed = Content.Load<Texture2D>("Button_Options_Unpressed");
            LoadedContent.menu_Button_Credits_Pressed = Content.Load<Texture2D>("Button_Credits_Pressed");
            LoadedContent.menu_Button_Credits_NotPressed = Content.Load<Texture2D>("Button_Credits_Unpressed");
            LoadedContent.menu_Button_Highscore_Pressed = Content.Load<Texture2D>("Button_Highscore_Pressed");
            LoadedContent.menu_Button_Highscore_NotPressed = Content.Load<Texture2D>("Button_Highscore_Unpressed");
            LoadedContent.menu_Button_Zuruck_Pressed = Content.Load<Texture2D>("Button_Back_Pressed");
            LoadedContent.menu_Button_Zuruck_NotPressed = Content.Load<Texture2D>("Button_Back_Unpressed");
            LoadedContent.menu_Button_Weiter_Pressed = Content.Load<Texture2D>("Button_Continue_Pressed");
            LoadedContent.menu_Button_Weiter_NotPressed = Content.Load<Texture2D>("Button_Continue_Unpressed");
            LoadedContent.menu_Button_Neustart_Pressed = Content.Load<Texture2D>("Button_Restart_Pressed");
            LoadedContent.menu_Button_Neustart_NotPressed = Content.Load<Texture2D>("Button_Restart_Unpressed");
            LoadedContent.menu_Button_Beenden_Pressed = Content.Load<Texture2D>("Button_Exit_Pressed");
            LoadedContent.menu_Button_Beenden_NotPressed = Content.Load<Texture2D>("Button_Exit_Unpressed");
            LoadedContent.menu_Credits_Additional_Background = Content.Load<Texture2D>("Credits_Additional_Background");

            LoadedContent.menu_Button_Player1_Pressed = Content.Load<Texture2D>("Button_Player_One_Pressed");
            LoadedContent.menu_Button_Player1_NotPressed = Content.Load<Texture2D>("Button_Player_One_Unpressed");
            LoadedContent.menu_Button_Player2_Pressed = Content.Load<Texture2D>("Button_Player_Two_Pressed");
            LoadedContent.menu_Button_Player2_NotPressed = Content.Load<Texture2D>("Button_Player_Two_Unpressed");
            LoadedContent.menu_Button_Keyboard = Content.Load<Texture2D>("ControlMode_Keyboard");
            LoadedContent.menu_Button_Gamepad1 = Content.Load<Texture2D>("ControlMode_Gamepad1");
            LoadedContent.menu_Button_Gamepad2 = Content.Load<Texture2D>("ControlMode_Gamepad2");

            LoadedContent.credits = Content.Load<Texture2D>("Credits");
            // TODO: use this.Content to load your game content here

            LoadedContent.reddot = Content.Load<Texture2D>("reddot");
            LoadedContent.orangedot = Content.Load<Texture2D>("orangedot");
            LoadedContent.dot = Content.Load<Texture2D>("dot");


            LoadedContent.font = Content.Load<SpriteFont>("Arial");


            GS_MAINMENU = new StateMainmenu(this);
            GS_SINGLEPLAYER = new StateSingleplayer(this, Player.CONTROLMODE_KEYBOARD);
            GS_MULTIPLAYER = new StateMultiplayer(this, Player.CONTROLMODE_KEYBOARD, Player.CONTROLMODE_GAMEPAD1);
            GS_OPTIONS = new StateOptions(this);
            GS_CREDITS = new StateCredits(this);
            GS_HIGHSCORE = new StateHighscore(this);
            GS_PAUSE = new StatePause(this, GS_SINGLEPLAYER, GS_MULTIPLAYER);

            LoadedContent.highscorePath = "Content\\highscores";
            LoadedContent.highscoreData = new HighscoreData();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (Keyboard.GetState().IsKeyDown(Keys.F10))
                this.Exit();

            float timeSinceLastFrame = (float)gameTime.ElapsedGameTime.TotalSeconds;

            GameState.keyState = Keyboard.GetState();
            GameState.mouseState = Mouse.GetState();
            GameState.gpState_Player1 = GamePad.GetState(PlayerIndex.One);
            GameState.gpState_Player2 = GamePad.GetState(PlayerIndex.Two);

            switch (GameState.currentGameState)
            {
                case (GameState.MENU):
                    GS_MAINMENU.update(timeSinceLastFrame);
                    break;
                case (GameState.SINGLEPLAYER):
                    GS_SINGLEPLAYER.update(timeSinceLastFrame);
                    break;
                case (GameState.MULTIPLAYER):
                    GS_MULTIPLAYER.update(timeSinceLastFrame);
                    break;
                case (GameState.OPTIONS):
                    GS_OPTIONS.update(timeSinceLastFrame);
                    break;
                case (GameState.CREDITS):
                    GS_CREDITS.update(timeSinceLastFrame);
                    break;
                case (GameState.HIGHSCORE):
                    GS_HIGHSCORE.update(timeSinceLastFrame);
                    break;
                case (GameState.PAUSE):
                    GS_PAUSE.update(timeSinceLastFrame);
                    break;

            }


            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            
            // TODO: Add your drawing code here
            spriteBatch.Begin(SpriteSortMode.Immediate, null);


            switch (GameState.currentGameState)
            {
                case (GameState.MENU):
                    GS_MAINMENU.draw(spriteBatch);
                    break;
                case (GameState.SINGLEPLAYER):
                    GS_SINGLEPLAYER.draw(spriteBatch);
                    break;
                case (GameState.MULTIPLAYER):
                    GS_MULTIPLAYER.draw(spriteBatch);
                    break;
                case (GameState.OPTIONS):
                    GS_OPTIONS.draw(spriteBatch);
                    break;
                case (GameState.CREDITS):
                    GS_CREDITS.draw(spriteBatch);
                    break;
                case (GameState.HIGHSCORE):
                    GS_HIGHSCORE.draw(spriteBatch);
                    break;
                case (GameState.PAUSE):
                    GS_PAUSE.draw(spriteBatch);
                    break;
            }

            spriteBatch.End();

            

            base.Draw(gameTime);
        }
    }
}
