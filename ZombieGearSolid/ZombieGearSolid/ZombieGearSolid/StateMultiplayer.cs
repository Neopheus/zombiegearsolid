﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZombieGearSolid
{
    public class StateMultiplayer : GameState
    {
        public Player player;
        public Player player2;

        private Random random = new Random();

        private Vector2[,] backgroundPos = new Vector2[3, 3];
        private Vector2 waffenverschiebung = new Vector2(3, 3);
        private Vector2 levelpos = new Vector2(5, 5);

        private Vector2[] heartPos = new Vector2[Player.FULLHP];
        private Vector2[] ammoPos = new Vector2[Player.FULLAMMO];

        private List<Obstacle> obstacles = new List<Obstacle>();
        private List<Bullet> bullets = new List<Bullet>();
        private List<EnemyHunter> enemyHunter = new List<EnemyHunter>();
        private List<EnemyRandom> enemyRandom = new List<EnemyRandom>();

        private int level = 0;

        private float timeSinceDeath = 0;
        private float timeSinceWin = 0;


        private const int OBSTACLES_SMALL = 50;
        private const int OBSTACLES_NORMAL = 40;
        private const int OBSTACLES_BIG = 30;
        private const int OBSTACLES_LARGE = 20;

        public StateMultiplayer(Game1 g1, int CONTROLMODE1, int CONTROLMODE2)
            : base(g1)
        {
            player = new Player(CONTROLMODE1, obstacles, bullets, enemyHunter, enemyRandom);
            player2 = new Player(CONTROLMODE2, obstacles, bullets, enemyHunter, enemyRandom);
            backgroundPos[0, 0] = new Vector2(0, 0);
            backgroundPos[0, 1] = new Vector2(1600, 0);
            backgroundPos[0, 2] = new Vector2(3200, 0);
            backgroundPos[1, 0] = new Vector2(0, 1600);
            backgroundPos[1, 1] = new Vector2(1600, 1600);
            backgroundPos[1, 2] = new Vector2(3200, 1600);
            backgroundPos[2, 0] = new Vector2(0, 3200);
            backgroundPos[2, 1] = new Vector2(1600, 3200);
            backgroundPos[2, 2] = new Vector2(3200, 3200);

            setzeObstacles(OBSTACLES_SMALL, OBSTACLES_NORMAL, OBSTACLES_BIG, OBSTACLES_LARGE);

            for (int i = 0; i < heartPos.Length; i++)
            {
                heartPos[i] = new Vector2(i * 24, 480 - 24 - 5);
            }

            for (int i = 0; i < Player.FULLAMMO; i++)
            {
                ammoPos[i] = new Vector2(800 - ((i + 1) * 11), 480 - 24 - 5);
            }

        }

        private void setzeObstacles(int anzahlSmall, int anzahlNormal, int anzahlBig, int anzahlLarge)
        {
            Random r = new Random();
            for (int i = 0; i < anzahlSmall; i++)
            {
                while (true)
                {
                    double x = r.NextDouble() * (4000 - 24 - 60) + 460;
                    double y = r.NextDouble() * (4000 - 24 - 60) + 460;
                    Obstacle o = new Obstacle(new Vector2((float)x, (float)y), Obstacle.SIZE_SMALL);
                    bool ueberschneidung = false;
                    for (int k = 0; k < obstacles.Count; k++)
                    {
                        if (o.bigBoundingBox.Intersects(obstacles[k].bigBoundingBox))
                        {
                            ueberschneidung = true;
                            break;
                        }
                    }
                    if (!ueberschneidung)
                    {
                        obstacles.Add(o);
                        break;
                    }
                }
            }

            for (int i = 0; i < anzahlNormal; i++)
            {
                while (true)
                {
                    double x = r.NextDouble() * (4000 - 48 - 60) + 460;
                    double y = r.NextDouble() * (4000 - 48 - 60) + 460;
                    Obstacle o = new Obstacle(new Vector2((float)x, (float)y), Obstacle.SIZE_NORMAL);
                    bool ueberschneidung = false;
                    for (int k = 0; k < obstacles.Count; k++)
                    {
                        if (o.bigBoundingBox.Intersects(obstacles[k].bigBoundingBox))
                        {
                            ueberschneidung = true;
                            break;
                        }
                    }
                    if (!ueberschneidung)
                    {
                        obstacles.Add(o);
                        break;
                    }
                }
            }

            for (int i = 0; i < anzahlBig; i++)
            {
                while (true)
                {
                    double x = r.NextDouble() * (4000 - 72 - 60) + 460;
                    double y = r.NextDouble() * (4000 - 72 - 60) + 460;
                    Obstacle o = new Obstacle(new Vector2((float)x, (float)y), Obstacle.SIZE_BIG);
                    bool ueberschneidung = false;
                    for (int k = 0; k < obstacles.Count; k++)
                    {
                        if (o.bigBoundingBox.Intersects(obstacles[k].bigBoundingBox))
                        {
                            ueberschneidung = true;
                            break;
                        }
                    }
                    if (!ueberschneidung)
                    {
                        obstacles.Add(o);
                        break;
                    }
                }
            }

            for (int i = 0; i < anzahlLarge; i++)
            {
                while (true)
                {
                    double x = r.NextDouble() * (4000 - 96 - 60) + 460;
                    double y = r.NextDouble() * (4000 - 96 - 60) + 460;
                    Obstacle o = new Obstacle(new Vector2((float)x, (float)y), Obstacle.SIZE_LARGE);
                    bool ueberschneidung = false;
                    for (int k = 0; k < obstacles.Count; k++)
                    {
                        if (o.bigBoundingBox.Intersects(obstacles[k].bigBoundingBox))
                        {
                            ueberschneidung = true;
                            break;
                        }
                    }
                    if (!ueberschneidung)
                    {
                        obstacles.Add(o);
                        break;
                    }
                }
            }
        }

        override public void update(float timeSinceLastFrame)
        {
            if (keyState.IsKeyDown(Keys.Escape) || gpState_Player1.Buttons.Start == ButtonState.Pressed)
            {
                g1.GS_PAUSE.currentState = StatePause.CURRENTMULTI;
                GameState.currentGameState = GameState.PAUSE;
            }

            player.Update(timeSinceLastFrame, GameState.keyState, GameState.gpState_Player1, GameState.gpState_Player2, GameState.mouseState);
            player2.Update(timeSinceLastFrame, GameState.keyState, GameState.gpState_Player1, GameState.gpState_Player2, GameState.mouseState);

            if (enemyHunter.Count <= 0)
            {
                for (int i = 0; i < enemyRandom.Count; i++)
                {
                    double zufall = random.NextDouble();
                    if (zufall < 0.5) enemyHunter.Add(new EnemyHunter(obstacles, bullets, enemyHunter, enemyRandom[i].position, player));
                    else enemyHunter.Add(new EnemyHunter(obstacles, bullets, enemyHunter, enemyRandom[i].position, player2));
                }

                enemyRandom.RemoveRange(0, enemyRandom.Count);
            }

            if (player.hp <= 0 && player2.hp <= 0)
            {
                timeSinceDeath += timeSinceLastFrame;
            }

            if ((player.hp > 0 || player2.hp > 0) && enemyHunter.Count == 0 && enemyRandom.Count == 0)
            {
                timeSinceWin += timeSinceLastFrame;
            }

            for (int i = 0; i < bullets.Count; i++)
            {
                bullets.ElementAt(i).update(timeSinceLastFrame);
            }

            for (int i = 0; i < enemyHunter.Count; i++)
            {
                enemyHunter[i].Update(timeSinceLastFrame);
            }

            for (int i = 0; i < enemyRandom.Count; i++)
            {
                enemyRandom[i].Update(timeSinceLastFrame);
            }

            if ((timeSinceWin > 2 && enemyHunter.Count <= 0 && enemyRandom.Count <= 0) || level == 0)
            {
                level++;

                timeSinceWin = 0;
                bullets.RemoveRange(0, bullets.Count);

                player.position = player.STARTPOSITION;
                player.hp = Player.FULLHP;
                player.ammo = Player.FULLAMMO;

                player2.position = player.STARTPOSITION;
                player2.hp = Player.FULLHP;
                player2.ammo = Player.FULLAMMO;


                for (int i = 0; i < level * level * 2; i++)
                {
                    float X;
                    float Y;
                    do
                    {
                        X = (4000 - 24) * (float)random.NextDouble() + 400;
                        Y = (4000 - 24) * (float)random.NextDouble() + 400;
                    } while ((X > player.STARTPOSITION.X - 400 && X < player.STARTPOSITION.X + 400) && (Y > player.STARTPOSITION.Y - 400 && Y < player.STARTPOSITION.Y + 400) && (X > player2.STARTPOSITION.X - 400 && X < player2.STARTPOSITION.X + 400) && (Y > player2.STARTPOSITION.Y - 400 && Y < player2.STARTPOSITION.Y + 400));

                    double zufall = random.NextDouble();
                    if (zufall < 0.5) enemyHunter.Add(new EnemyHunter(obstacles, bullets, enemyHunter, new Vector2(X, Y), player));
                    else enemyHunter.Add(new EnemyHunter(obstacles, bullets, enemyHunter, new Vector2(X, Y), player2));
                }
                for (int i = 0; i < level * level; i++)
                {
                    float X;
                    float Y;
                    do
                    {
                        X = (4000 - 24) * (float)random.NextDouble() + 400;
                        Y = (4000 - 24) * (float)random.NextDouble() + 400;
                    } while ((X > player.STARTPOSITION.X - 400 && X < player.STARTPOSITION.X + 400) && (Y > player.STARTPOSITION.Y - 400 && Y < player.STARTPOSITION.Y + 400) && (X > player2.STARTPOSITION.X - 400 && X < player2.STARTPOSITION.X + 400) && (Y > player2.STARTPOSITION.Y - 400 && Y < player2.STARTPOSITION.Y + 400));


                    enemyRandom.Add(new EnemyRandom(obstacles, bullets, enemyRandom, new Vector2(X, Y)));
                }
            }

            if (player.hp == 0 && player2.hp == 0 && (gpState_Player1.Buttons.B == ButtonState.Pressed || gpState_Player2.Buttons.B == ButtonState.Pressed || keyState.IsKeyDown(Keys.Space)))
            {
                restart();
            }

            if (player.hp <= 0 && player2.hp <= 0) LoadedContent.gameOver = true;
            else LoadedContent.gameOver = false;
        }

        public void restart()
        {
            player.position = player.STARTPOSITION;
            player2.position = player2.STARTPOSITION;
            level = 0;
            player.hp = Player.FULLHP;
            player.ammo = Player.FULLAMMO;
            player2.hp = Player.FULLHP;
            player2.ammo = Player.FULLAMMO;
            enemyHunter.RemoveRange(0, enemyHunter.Count);
            enemyRandom.RemoveRange(0, enemyRandom.Count);
            bullets.RemoveRange(0, bullets.Count);
            timeSinceDeath = 0;

            obstacles.RemoveRange(0, obstacles.Count);

            setzeObstacles(OBSTACLES_SMALL, OBSTACLES_NORMAL, OBSTACLES_BIG, OBSTACLES_LARGE);
        }


        override public void draw(SpriteBatch spriteBatch)
        {
            Viewport up = new Viewport(0, 0, 800, 480);
            Viewport down = new Viewport(0, 480, 800, 480);



            g1.GraphicsDevice.Viewport = up;

            spriteBatch.Draw(LoadedContent.background[0, 0], backgroundPos[0, 0] - player.position + LoadedContent.defaultRootPosition, Color.White);
            spriteBatch.Draw(LoadedContent.background[0, 1], backgroundPos[0, 1] - player.position + LoadedContent.defaultRootPosition, Color.White);
            spriteBatch.Draw(LoadedContent.background[0, 2], backgroundPos[0, 2] - player.position + LoadedContent.defaultRootPosition, Color.White);

            spriteBatch.Draw(LoadedContent.background[1, 0], backgroundPos[1, 0] - player.position + LoadedContent.defaultRootPosition, Color.White);
            spriteBatch.Draw(LoadedContent.background[1, 1], backgroundPos[1, 1] - player.position + LoadedContent.defaultRootPosition, Color.White);
            spriteBatch.Draw(LoadedContent.background[1, 2], backgroundPos[1, 2] - player.position + LoadedContent.defaultRootPosition, Color.White);

            spriteBatch.Draw(LoadedContent.background[2, 0], backgroundPos[2, 0] - player.position + LoadedContent.defaultRootPosition, Color.White);
            spriteBatch.Draw(LoadedContent.background[2, 1], backgroundPos[2, 1] - player.position + LoadedContent.defaultRootPosition, Color.White);
            spriteBatch.Draw(LoadedContent.background[2, 2], backgroundPos[2, 2] - player.position + LoadedContent.defaultRootPosition, Color.White);


            for (int i = 0; i < bullets.Count; i++)
            {
                spriteBatch.Draw(bullets.ElementAt(i).look, bullets.ElementAt(i).position - player.position + LoadedContent.defaultRootPosition, Color.White);
            }


            spriteBatch.Draw(player2.weaponlook, player2.position - waffenverschiebung - player.position + LoadedContent.defaultRootPosition, Color.White);

            spriteBatch.Draw(player2.look, player2.position - player.position + LoadedContent.defaultRootPosition, Color.White);

            if (player2.hp <= 0)
            {
                spriteBatch.Draw(LoadedContent.player_Dead, player2.position - player.position + LoadedContent.defaultRootPosition, Color.White);
            }

            spriteBatch.Draw(player.weaponlook, LoadedContent.defaultRootPosition - waffenverschiebung, Color.White);

            spriteBatch.Draw(player.look, LoadedContent.defaultRootPosition, Color.White);


            if (player.hp <= 0)
            {
                spriteBatch.Draw(LoadedContent.player_Dead, LoadedContent.defaultRootPosition, Color.White);
            }


            for (int i = 0; i < enemyHunter.Count; i++)
            {
                spriteBatch.Draw(enemyHunter[i].look, enemyHunter[i].position - player.position + LoadedContent.defaultRootPosition, Color.White);
            }


            for (int i = 0; i < enemyRandom.Count; i++)
            {
                spriteBatch.Draw(enemyRandom[i].look, enemyRandom[i].position - player.position + LoadedContent.defaultRootPosition, Color.White);
            }


            for (int i = 0; i < obstacles.Count; i++)
            {
                spriteBatch.Draw(obstacles[i].look, obstacles[i].position - player.position + LoadedContent.defaultRootPosition, Color.White);
            }


            spriteBatch.Draw(LoadedContent.shadow, Vector2.Zero, Color.White);


            for (int i = 0; i < Player.FULLHP; i++)
            {
                if (player.hp > i)
                {
                    spriteBatch.Draw(LoadedContent.hud_Heart_Full, heartPos[i], Color.White);
                }
                else
                {
                    spriteBatch.Draw(LoadedContent.hud_Heart_Empty, heartPos[i], Color.White);
                }
            }


            for (int i = 0; i < enemyRandom.Count; i++)
            {
                Vector2 radarpos = enemyRandom[i].position - player.position;
                float entfernung = radarpos.Length();
                radarpos = Vector2.Normalize(radarpos);
                radarpos *= 100;
                if (entfernung < 2000)
                {
                    radarpos = radarpos * entfernung / 2000;
                }
                spriteBatch.Draw(LoadedContent.dot, LoadedContent.defaultRootPosition + new Vector2(12, 12) + radarpos, Color.Blue);
            }


            for (int i = 0; i < enemyHunter.Count; i++)
            {
                Vector2 radarpos = enemyHunter[i].position - player.position;
                float entfernung = radarpos.Length();
                radarpos = Vector2.Normalize(radarpos);
                radarpos *= 100;
                if (entfernung < 2000)
                {
                    radarpos = radarpos * entfernung / 2000;
                }
                spriteBatch.Draw(LoadedContent.dot, LoadedContent.defaultRootPosition + new Vector2(12, 12) + radarpos, Color.Red);
            }


            if (player.controlMode == Player.CONTROLMODE_KEYBOARD)
            {
                spriteBatch.Draw(LoadedContent.player_Fadenkreuz, player.maus, Color.White);
            }

            if (player.controlMode == Player.CONTROLMODE_GAMEPAD1 || player.controlMode == Player.CONTROLMODE_GAMEPAD2)
            {
                Vector2 fadenkreuzVector = new Vector2(player.schussVector.X, player.schussVector.Y * -1);
                spriteBatch.Draw(LoadedContent.player_Fadenkreuz, LoadedContent.defaultRootPosition + Vector2.Normalize(fadenkreuzVector) * 75, Color.White);
            }


            for (int i = 0; i < player.ammo; i++)
            {
                spriteBatch.Draw(LoadedContent.hud_Ammo, ammoPos[i], Color.White);
            }


            if (timeSinceDeath > 2)
            {
                spriteBatch.Draw(LoadedContent.hud_Text_Lose, Vector2.Zero, Color.White);
            }

            if (timeSinceWin > 0 && level > 0)
            {
                spriteBatch.Draw(LoadedContent.hud_Text_Win, Vector2.Zero, Color.White);
            }


            spriteBatch.DrawString(LoadedContent.font, "Level " + level, levelpos, Color.White);


            spriteBatch.DrawString(LoadedContent.font, "" + ((level * level * 3) - enemyHunter.Count - enemyRandom.Count) + "/" + (level * level * 3), new Vector2(700, 5), Color.White);


            //spriteBatch.DrawString(LoadedContent.font, "" + player.position.X + " " + player.position.Y, new Vector2(20, 20), Color.White);
            //spriteBatch.DrawString(LoadedContent.font, "" + bullets.Count, new Vector2(40, 40), Color.White);
            //spriteBatch.DrawString(LoadedContent.font, "" + player.speed.X + " " + player.speed.Y, new Vector2(100, 100), Color.White);
            //spriteBatch.DrawString(LoadedContent.font, "" + player.maus.X + " " + player.maus.Y, new Vector2(100, 100), Color.White);



            g1.GraphicsDevice.Viewport = down;

            spriteBatch.Draw(LoadedContent.background[0, 0], backgroundPos[0, 0] - player2.position + LoadedContent.defaultRootPosition, Color.White);
            spriteBatch.Draw(LoadedContent.background[0, 1], backgroundPos[0, 1] - player2.position + LoadedContent.defaultRootPosition, Color.White);
            spriteBatch.Draw(LoadedContent.background[0, 2], backgroundPos[0, 2] - player2.position + LoadedContent.defaultRootPosition, Color.White);

            spriteBatch.Draw(LoadedContent.background[1, 0], backgroundPos[1, 0] - player2.position + LoadedContent.defaultRootPosition, Color.White);
            spriteBatch.Draw(LoadedContent.background[1, 1], backgroundPos[1, 1] - player2.position + LoadedContent.defaultRootPosition, Color.White);
            spriteBatch.Draw(LoadedContent.background[1, 2], backgroundPos[1, 2] - player2.position + LoadedContent.defaultRootPosition, Color.White);

            spriteBatch.Draw(LoadedContent.background[2, 0], backgroundPos[2, 0] - player2.position + LoadedContent.defaultRootPosition, Color.White);
            spriteBatch.Draw(LoadedContent.background[2, 1], backgroundPos[2, 1] - player2.position + LoadedContent.defaultRootPosition, Color.White);
            spriteBatch.Draw(LoadedContent.background[2, 2], backgroundPos[2, 2] - player2.position + LoadedContent.defaultRootPosition, Color.White);


            for (int i = 0; i < bullets.Count; i++)
            {
                spriteBatch.Draw(bullets.ElementAt(i).look, bullets.ElementAt(i).position - player2.position + LoadedContent.defaultRootPosition, Color.White);
            }


            spriteBatch.Draw(player.weaponlook, player.position - waffenverschiebung - player2.position + LoadedContent.defaultRootPosition, Color.White);

            spriteBatch.Draw(player.look, player.position - player2.position + LoadedContent.defaultRootPosition, Color.White);

            if (player.hp <= 0)
            {
                spriteBatch.Draw(LoadedContent.player_Dead, player.position - player2.position + LoadedContent.defaultRootPosition, Color.White);
            }

            spriteBatch.Draw(player2.weaponlook, LoadedContent.defaultRootPosition - waffenverschiebung, Color.White);

            spriteBatch.Draw(player2.look, LoadedContent.defaultRootPosition, Color.White);


            if (player2.hp <= 0)
            {
                spriteBatch.Draw(LoadedContent.player_Dead, LoadedContent.defaultRootPosition, Color.White);
            }


            for (int i = 0; i < enemyHunter.Count; i++)
            {
                spriteBatch.Draw(enemyHunter[i].look, enemyHunter[i].position - player2.position + LoadedContent.defaultRootPosition, Color.White);
            }


            for (int i = 0; i < enemyRandom.Count; i++)
            {
                spriteBatch.Draw(enemyRandom[i].look, enemyRandom[i].position - player2.position + LoadedContent.defaultRootPosition, Color.White);
            }


            for (int i = 0; i < obstacles.Count; i++)
            {
                spriteBatch.Draw(obstacles[i].look, obstacles[i].position - player2.position + LoadedContent.defaultRootPosition, Color.White);
            }


            spriteBatch.Draw(LoadedContent.shadow, Vector2.Zero, Color.White);


            for (int i = 0; i < Player.FULLHP; i++)
            {
                if (player2.hp > i)
                {
                    spriteBatch.Draw(LoadedContent.hud_Heart_Full, heartPos[i], Color.White);
                }
                else
                {
                    spriteBatch.Draw(LoadedContent.hud_Heart_Empty, heartPos[i], Color.White);
                }
            }


            for (int i = 0; i < enemyRandom.Count; i++)
            {
                Vector2 radarpos = enemyRandom[i].position - player2.position;
                float entfernung = radarpos.Length();
                radarpos = Vector2.Normalize(radarpos);
                radarpos *= 100;
                if (entfernung < 2000)
                {
                    radarpos = radarpos * entfernung / 2000;
                }
                spriteBatch.Draw(LoadedContent.dot, LoadedContent.defaultRootPosition + new Vector2(12, 12) + radarpos, Color.Blue);
            }


            for (int i = 0; i < enemyHunter.Count; i++)
            {
                Vector2 radarpos = enemyHunter[i].position - player2.position;
                float entfernung = radarpos.Length();
                radarpos = Vector2.Normalize(radarpos);
                radarpos *= 100;
                if (entfernung < 2000)
                {
                    radarpos = radarpos * entfernung / 2000;
                }
                spriteBatch.Draw(LoadedContent.dot, LoadedContent.defaultRootPosition + new Vector2(12, 12) + radarpos, Color.Red);
            }


            if (player2.controlMode == Player.CONTROLMODE_KEYBOARD)
            {
                spriteBatch.Draw(LoadedContent.player_Fadenkreuz, player2.maus, Color.White);
            }

            if (player2.controlMode == Player.CONTROLMODE_GAMEPAD1 || player.controlMode == Player.CONTROLMODE_GAMEPAD2)
            {
                Vector2 fadenkreuzVector = new Vector2(player2.schussVector.X, player2.schussVector.Y * -1);
                spriteBatch.Draw(LoadedContent.player_Fadenkreuz, LoadedContent.defaultRootPosition + Vector2.Normalize(fadenkreuzVector) * 75, Color.White);
            }


            for (int i = 0; i < player2.ammo; i++)
            {
                spriteBatch.Draw(LoadedContent.hud_Ammo, ammoPos[i], Color.White);
            }


            if (timeSinceDeath > 2)
            {
                spriteBatch.Draw(LoadedContent.hud_Text_Lose, Vector2.Zero, Color.White);
            }

            if (timeSinceWin > 0 && level > 0)
            {
                spriteBatch.Draw(LoadedContent.hud_Text_Win, Vector2.Zero, Color.White);
            }


            spriteBatch.DrawString(LoadedContent.font, "Level " + level, levelpos, Color.White);


            spriteBatch.DrawString(LoadedContent.font, "" + ((level * level * 3) - enemyHunter.Count - enemyRandom.Count) + "/" + (level * level * 3), new Vector2(700, 5), Color.White);


            //spriteBatch.DrawString(LoadedContent.font, "" + player.position.X + " " + player.position.Y, new Vector2(20, 20), Color.White);
            //spriteBatch.DrawString(LoadedContent.font, "" + bullets.Count, new Vector2(40, 40), Color.White);
            //spriteBatch.DrawString(LoadedContent.font, "" + player.speed.X + " " + player.speed.Y, new Vector2(100, 100), Color.White);
            //spriteBatch.DrawString(LoadedContent.font, "" + player.maus.X + " " + player.maus.Y, new Vector2(100, 100), Color.White);
        }
    }
}