﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ZombieGearSolid
{
    public class EnemyRandom
    {
        private const float NEEDEDANITIME = 0.5f;
        private const int DIRECTION_UP = 0;
        private const int DIRECTION_DOWN = 1;
        private const int DIRECTION_LEFT = 2;
        private const int DIRECTION_RIGHT = 3;
        private int BASICSPEED = 200;

        private static Random random = new Random();

        public Rectangle boundingBox;
        private int direction;
        public Vector2 speed = new Vector2(0, 0);
        public Vector2 position;
        public Vector2 prePosition;
        private float anitime = 0;
        private float nextDirectionChange = 1;
        private float timeSinceLastDirectionChange = 1;

        private List<Obstacle> obstacles;
        private List<Bullet> bullets;
        private List<EnemyRandom> enemyRandom;

        public Texture2D look
        {
            get
            {
                switch (direction)
                {
                    case (DIRECTION_UP):
                        if (speed == Vector2.Zero)
                        {
                            return LoadedContent.enemyRandom_Stop_Up;
                        }
                        else
                        {
                            if (anitime <= (NEEDEDANITIME / 2))
                            {
                                return LoadedContent.enemyRandom_Walk_Up_1;
                            }
                            else return LoadedContent.enemyRandom_Walk_Up_2;
                        }
                    case (DIRECTION_DOWN):
                        if (speed == Vector2.Zero)
                        {
                            return LoadedContent.enemyRandom_Stop_Down;
                        }
                        else
                        {
                            if (anitime <= (NEEDEDANITIME / 2))
                            {
                                return LoadedContent.enemyRandom_Walk_Down_1;
                            }
                            else return LoadedContent.enemyRandom_Walk_Down_2;
                        }
                    case (DIRECTION_LEFT):
                        if (speed == Vector2.Zero)
                        {
                            return LoadedContent.enemyRandom_Stop_Left;
                        }
                        else
                        {
                            if (anitime <= (NEEDEDANITIME / 2))
                            {
                                return LoadedContent.enemyRandom_Walk_Left_1;
                            }
                            else return LoadedContent.enemyRandom_Walk_Left_2;
                        }
                    case (DIRECTION_RIGHT):
                        if (speed == Vector2.Zero)
                        {
                            return LoadedContent.enemyRandom_Stop_Right;
                        }
                        else
                        {
                            if (anitime <= (NEEDEDANITIME / 2))
                            {
                                return LoadedContent.enemyRandom_Walk_Right_1;
                            }
                            else return LoadedContent.enemyRandom_Walk_Right_2;
                        }

                    default: return null;
                }
            }
        }

        public EnemyRandom(List<Obstacle> obstacles, List<Bullet> bullets, List<EnemyRandom> enemyRandom, Vector2 position)
        {
            BASICSPEED += (int)(random.NextDouble() * 100);
            this.position = position;
            prePosition = position;
            this.enemyRandom = enemyRandom;
            this.bullets = bullets;
            boundingBox = new Rectangle((int)position.X, (int)position.Y, look.Width, look.Height);
            speed = new Vector2(0, 0);
            this.obstacles = obstacles;
        }

        public void Update(float timeSinceLastFrame)
        {
            anitime += timeSinceLastFrame;
            timeSinceLastDirectionChange += timeSinceLastFrame;

            if (anitime > NEEDEDANITIME) anitime = 0;

            if (timeSinceLastDirectionChange >= nextDirectionChange)
            {
                timeSinceLastDirectionChange = 0;
                nextDirectionChange = (float)random.NextDouble() * 5 + 2;
                float randomX = (float)random.NextDouble() * 2 - 1;
                float randomY = (float)random.NextDouble() * 2 - 1;
                speed.X = randomX;
                speed.Y = randomY;
                speed = Vector2.Normalize(speed);
                speed *= BASICSPEED;
            }

            if (LoadedContent.gameOver) speed /= 10;
            

            position.X += speed.X * timeSinceLastFrame;
            position.Y += speed.Y * timeSinceLastFrame;

            if (position.X < 400)
            {
                position.X = 400;
                speed.X *= -1;
            }
            if (position.Y < 400)
            {
                position.Y = 400;
                speed.Y *= -1;
            }

            if (position.X > 4400 - 24)
            {
                position.X = 4400 - 24;
                speed.X *= -1;
            }
            if (position.Y > 4400 - 24)
            {
                position.Y = 4400 - 24;
                speed.Y *= -1;
            }


            float betragSpeedX = speed.X;
            float betragSpeedY = speed.Y;
            if (betragSpeedX < 0) betragSpeedX *= -1;
            if (betragSpeedY < 0) betragSpeedY *= -1;

            if (betragSpeedX > betragSpeedY)
            {
                if (speed.X < 0) direction = DIRECTION_LEFT;
                else direction=DIRECTION_RIGHT;
            }
            else if (betragSpeedY > betragSpeedX)
            {
                if (speed.Y < 0) direction = DIRECTION_UP;
                else direction = DIRECTION_DOWN;
            }

            boundingBox.X = (int)position.X;
            boundingBox.Y = (int)position.Y;

            for (int i = 0; i < bullets.Count; i++)
            {
                if (boundingBox.Intersects(bullets[i].boundingBox))
                {
                    bullets.Remove(bullets[i]);
                    enemyRandom.Remove(this);
                }
            }


            bool changed = true;
            while (changed)
            {
                changed = false;
                for (int i = 0; i < obstacles.Count; i++)
                {
                    if (boundingBox.Intersects(obstacles[i].boundingBox))
                    {
                        changed = true;
                        //LEFT
                        if (prePosition.X < obstacles[i].position.X && prePosition.Y < obstacles[i].position.Y + obstacles[i].boundingBox.Height && prePosition.X - obstacles[i].position.X < prePosition.Y - obstacles[i].position.Y)
                        {
                            position.X = obstacles[i].position.X - 24;
                            speed.X = 0;
                            boundingBox = new Rectangle((int)position.X, (int)position.Y, 24, 24);
                        }
                        //UP
                        else if (prePosition.Y < obstacles[i].position.Y && prePosition.X < obstacles[i].position.X + obstacles[i].boundingBox.Width)
                        {
                            position.Y = obstacles[i].position.Y - 24;
                            speed.Y = 0;
                            boundingBox = new Rectangle((int)position.X, (int)position.Y, 24, 24);
                        }
                        //RIGHT
                        else if (prePosition.X >= obstacles[i].position.X + obstacles[i].boundingBox.Width)
                        {
                            position.X = obstacles[i].position.X + obstacles[i].boundingBox.Width;
                            speed.X = 0;
                            boundingBox = new Rectangle((int)position.X, (int)position.Y, 24, 24);
                        }
                        //DOWN
                        else
                        {
                            position.Y = obstacles[i].position.Y + obstacles[i].boundingBox.Height;
                            speed.Y = 0;
                            boundingBox = new Rectangle((int)position.X, (int)position.Y, 24, 24);
                        }
                    }
                }
            }







            //MUSS ANS ENDE!!!
            prePosition = position;
        }
    }
}
