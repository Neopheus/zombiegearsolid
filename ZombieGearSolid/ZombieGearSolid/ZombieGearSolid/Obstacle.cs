﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ZombieGearSolid
{
    public class Obstacle
    {
        public Vector2 position;
        public Rectangle bigBoundingBox;
        public Rectangle boundingBox;
        public Texture2D look;
        public const int SIZE_SMALL = 0;
        public const int SIZE_NORMAL = 1;
        public const int SIZE_BIG = 2;
        public const int SIZE_LARGE = 3;
        public static Random r = new Random();

        public Obstacle(Vector2 position, int sizeMode)
        {
            this.position = position;
            double zufall = r.NextDouble();
            switch (sizeMode)
            {
                case(SIZE_SMALL):
                    boundingBox = new Rectangle((int)position.X, (int)position.Y, 24, 24);
                    bigBoundingBox = new Rectangle((int)position.X -30, (int)position.Y -30, 24+60, 24+60);
                    if (zufall < 0.5)
                        look = LoadedContent.box_Small;
                    else 
                        look = LoadedContent.stone_Small;
                    break;
                case(SIZE_NORMAL):
                    boundingBox = new Rectangle((int)position.X, (int)position.Y, 48, 48);
                    bigBoundingBox = new Rectangle((int)position.X - 30, (int)position.Y - 30, 48 + 60, 48 + 60);
                    if (zufall < 0.33)
                        look = LoadedContent.box_Normal;
                    else if (zufall < 0.66)
                        look = LoadedContent.stone_Normal;
                    else
                        look = LoadedContent.bush_Normal;
                    break;
                case(SIZE_BIG):
                    boundingBox = new Rectangle((int)position.X, (int)position.Y, 72, 72);
                    bigBoundingBox = new Rectangle((int)position.X - 30, (int)position.Y - 30, 72 + 60, 72 + 60);
                    if (zufall < 0.5)
                        look = LoadedContent.bush_Big;
                    else
                        look = LoadedContent.stone_Big;
                    break;
                case(SIZE_LARGE):
                    boundingBox = new Rectangle((int)position.X, (int)position.Y, 96, 96);
                    bigBoundingBox = new Rectangle((int)position.X - 30, (int)position.Y - 30, 96 + 60, 96 + 60);
                    look = LoadedContent.stone_Large;
                    break;
            }
        }
    }
}
