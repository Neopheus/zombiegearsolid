﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ZombieGearSolid
{
    public class Bullet
    {
        public Vector2 position;
        public const int BASICSPEED = 650;
        private Vector2 speed;
        private List<Bullet> bullets = new List<Bullet>();
        private List<Obstacle> obstacles;
        private float TIMEFORONESPIN = 0.2f;
        private float anitime = 0;

        public Rectangle boundingBox;

        public Texture2D look
        {
            get
            {
                
                if (anitime < TIMEFORONESPIN / 4)
                {
                    return LoadedContent.bullet_1;
                }
                else if (anitime < TIMEFORONESPIN / 4 * 2)
                {
                    return LoadedContent.bullet_2;
                }
                else if (anitime < TIMEFORONESPIN / 4 * 3)
                {
                    return LoadedContent.bullet_3;
                }
                else
                {
                    return LoadedContent.bullet_4;
                }
            }
        }

        public Bullet(Vector2 position, Vector2 speed, List<Bullet> bullets, List<Obstacle> obstacles)
        {
            this.position = position;
            this.speed = speed;
            this.bullets = bullets;
            boundingBox = new Rectangle((int)position.X, (int)position.Y, 8, 8);
            this.obstacles = obstacles;
        }

        public void update(float timeSinceLastFrame)
        {
            position += speed * timeSinceLastFrame;
            if (position.X < 400 - 8) bullets.Remove(this);
            if (position.Y < 400 - 8) bullets.Remove(this);
            if (position.X > 4400) bullets.Remove(this);
            if (position.Y > 4400) bullets.Remove(this);

            for (int i = 0; i < obstacles.Count; i++)
            {
                if (boundingBox.Intersects(obstacles[i].boundingBox))
                {
                    bullets.Remove(this);
                    break;
                }
            }

            anitime += timeSinceLastFrame;
            if (anitime > TIMEFORONESPIN) anitime = 0;

            boundingBox.X = (int)position.X;
            boundingBox.Y = (int)position.Y;
        }
    }
}
